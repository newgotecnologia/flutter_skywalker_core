import 'package:flutter/material.dart';
import 'package:flutter_skywalker_core/src/widgets/sky_text.dart';

typedef String OptionTitleResolver<T>(T t);

String _defaultTitleResolver(Object? val) {
  return val != null ? val.toString() : '';
}

class SkyDropdownButton<T> extends StatefulWidget {
  final List<T> items;
  final Color textColor;
  final Color dropdownIconColor;
  final double fontSize;
  final OptionTitleResolver<T> resolver;
  final Function onChanged;
  final T? currentItem;
  final bool createFriendlyFirstItem;
  final bool isExpanded;
  final bool isDense;
  final bool enable;
  final String friendlyFirstItemText;
  final bool isAndroid;

  const SkyDropdownButton({
    Key? key,
    required this.items,
    this.resolver = _defaultTitleResolver,
    this.textColor = Colors.black,
    this.dropdownIconColor = Colors.blue,
    this.fontSize = 15,
    required this.onChanged,
    required this.currentItem,
    this.createFriendlyFirstItem = false,
    this.isExpanded = false,
    this.isDense = true,
    required this.friendlyFirstItemText,
    this.enable = true,
    this.isAndroid = true,
  }) : super(key: key);

  @override
  _SkyDropdownButtonState createState() => _SkyDropdownButtonState<T>(
        enable: enable,
        items: items,
        textColor: textColor,
        fontSize: fontSize,
        dropdownIconColor: dropdownIconColor,
        resolver: resolver,
        onChanged: onChanged,
        currentItem: currentItem,
        isExpanded: isExpanded ?? false,
        isDense: isDense ?? true,
        isAndroid: isAndroid ?? true,
        createFriendlyFirstItem: createFriendlyFirstItem ?? false,
        friendlyFirstItemText: friendlyFirstItemText,
      );
}

class _SkyDropdownButtonState<T> extends State<SkyDropdownButton<T>> {
  final List<T> items;
  final Color textColor;
  final Color dropdownIconColor;
  final double fontSize;
  final OptionTitleResolver<T> resolver;
  final Function onChanged;
  final bool isExpanded;
  final bool isDense;
  final bool enable;
  final bool createFriendlyFirstItem;
  final String friendlyFirstItemText;
  final bool isAndroid;

  late List<DropdownMenuItem<T>> _dropDownMenuItems;
  late T? currentItem;

  _SkyDropdownButtonState({
    required this.enable,
    required this.onChanged,
    required this.items,
    required this.textColor,
    required this.fontSize,
    required this.dropdownIconColor,
    required this.resolver,
    required this.isExpanded,
    required this.isDense,
    this.currentItem,
    required this.createFriendlyFirstItem,
    required this.friendlyFirstItemText,
    this.isAndroid = true,
  }) : assert(null != resolver);

  @override
  void initState() {
    _dropDownMenuItems = _buildDropdownItems();

    if (!createFriendlyFirstItem) {
      currentItem = currentItem ?? _dropDownMenuItems[0].value as T;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      isExpanded: isExpanded,
      items: _dropDownMenuItems,
      value: currentItem,
      onChanged: changedDropDownItem,
      icon: Icon(
        Icons.keyboard_arrow_down,
        color: dropdownIconColor,
      ),
      style: TextStyle(fontSize: fontSize, color: textColor),
      isDense: isDense,
    );
  }

  List<DropdownMenuItem<T>> _buildDropdownItems() {
    List<DropdownMenuItem<T>> items = [];

    if (this.createFriendlyFirstItem) {
      items.add(_createDropdownMenuItem(null, title: friendlyFirstItemText));
    }

    for (T item in this.items) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(_createDropdownMenuItem(item));
    }
    return items;
  }

  DropdownMenuItem<T> _createDropdownMenuItem(
    T? item, {
    String? title,
  }) =>
      DropdownMenuItem(
          value: item,
          child: item != null
              ? SkyText(
                  resolver(item),
                  textColor: textColor,
                  fontSize: fontSize,
                )
              : SkyText(
                  title!,
                  textColor: textColor,
                  fontSize: fontSize,
                ));

  void changedDropDownItem(T? selectedOption) {
    if (enable) {
      onChanged(selectedOption);
      setState(() {
        currentItem = selectedOption!;
      });
    }
  }

  void onSelectedItemChanged(int index) {
    changedDropDownItem(items[index]);
  }
}
