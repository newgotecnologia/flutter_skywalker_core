import 'package:flutter/material.dart';
import 'package:flutter_skywalker_core/src/widgets/sky_text.dart';

class SkyFlatButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final double fontSize;
  final int maxLines;
  final Color textColor;
  final Color buttonColor;

  const SkyFlatButton({
    Key? key,
    required this.onPressed,
    required this.text,
    this.fontSize = 12,
    this.maxLines = 1,
    this.textColor = Colors.white,
    this.buttonColor = Colors.blue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(buttonColor),
        elevation: MaterialStateProperty.all(0),
      ),
      child: SkyText(
        text,
        fontSize: fontSize,
        textColor: textColor,
      ),
      onPressed: onPressed,
    );
  }
}
